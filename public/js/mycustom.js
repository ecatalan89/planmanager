$(document).ready(function(){
  show_exported();
  $('#export').click(function(){
      $('.download').show('slow');
    return false;
  });

  $('#download').click(function(){

    var jqxhr = $.ajax( "export" )
      .done(function(data) {
        window.location.replace(data);
        show_exported();
      });



    return false;
  });

  /*generate exported files*/
  function show_exported(){
    // $.ajax( "exported" )
    //
    //   .done(function(data) {
    //     var html  ='';
    //
    //   });

      $.ajax({
      url: 'exported',
      async: false, //blocks window close
      success: function(data) {
        var html  ='';
        $.each( data, function( key, value ) {
          html += '<li class="list-group-item"><a href="'+value['file']+'">'+value['file']+'</a></li>';
        });
        $('#exported_files').html(html);
        }
      });


  }



});
