<head>
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/mycustom.css') }}" rel="stylesheet">
</head>
<?php
// echo "<pre>";
// print_r($companies);
// echo "</pre>";
?>


<div class="row">
  <div class="col-sm-3">

  </div>
  <div class="col-sm-6">

    <div class="actions">
      <div class="download">CSV File ready to download <button type="button" class="btn btn-danger" id="download">Download CSV</button></div>
      <div><button type="button" class="btn btn-success" id="export">Export To CSV</button></div>
    </div>

    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Created</th>
            <th>Number of Employees</th>
          </tr>
        </thead>
        <tbody>
          @foreach($companies as $key=>$company)
          <tr>
          <td>{{ $company->comp_id }}</td>
          <td>{{ $company->comp_name }}</td>
          <td>{{ $company->created }}</td>
          <td>{{ $count[$key] }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
      </div>

      <br><br>
      <hr>
      <h5>Exported Files</h3>

    <ul class="list-group" id="exported_files">
    </ul>

  </div>
  <div class="col-sm-3"></div>
</div>




<body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="{{ asset('js/mycustom.js') }}"></script>
</body>
