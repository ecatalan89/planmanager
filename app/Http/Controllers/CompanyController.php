<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use Illuminate\Support\Facades\DB;

use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;
use SplTempFileObject;
use PDO;
use Response;

class CompanyController extends Controller
{

  public function index()
    {
        $companies = DB::table('companies')->select('comp_id','comp_name','created')->get()->toArray();

        foreach($companies as $company){
          $compid = $company->comp_id;
          $count[] = DB::table('employees')->select('name')->where('comp_id',$compid)->get()->count();

        }

        return view('templates.index',compact('companies','count'));
    }

  public function show($id)
    {
      $company = Company::findOrFail($id);
      return $company;

    }

  public function export(){
    $dbh = DB::connection()->getPdo();

    $sth = $dbh->prepare(
        "SELECT e.name,e.id,c.comp_name,c.comp_id,e.date_created
        FROM employees e left join companies c
        on c.comp_id = e.comp_id"
    );

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $sth->execute();

    $file = "exports/".time().".employees.csv";
    $csv = Writer::createFromPath($file, "w");
    echo $file;

    DB::table('exports')->insert(
      ['file' => $file]
    );



    $csv->insertOne(['Employee', 'EmployeeID','Company','Company ID','Created']);

    $csv->insertAll($sth);

    //download in browser
    //$csv->output('users.csv');
    die;

  }

  public function exported(){
    $files = DB::table('exports')->select('file')->get()->toArray();

    return $files;
  }

}
?>
