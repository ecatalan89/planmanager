<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $dates = ['created_at'];

    protected $fillable = [
            'id',
            'name',
            'created_at'
        ];
}
