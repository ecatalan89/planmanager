<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/*Index*/
Route::get('/', 'CompanyController@index');

/*export csv*/
Route::get('export', 'CompanyController@export');

/*exported files*/
Route::get('exported', 'CompanyController@exported');

// Route::get('company/{id}', 'CompanyController@show');

// /*store*/
// Route::post('post', 'PostController@store');
